# Roadmap

This is the public road map for our project. We'll be keeping this page up to date with the latest information you can also look at our [website for more detailed weekly updates](http://manaengine.gitlab.io/). 

The issues in this repo are features we are working on so you can keep track of our progress or even submit your own feature request! You can also check out the [Board](https://gitlab.com/manaengine/roadmap/-/boards) for a better overview of what we have planned and what we have alread finished.

## High Level Overview
This is meant to be our highlevel plan on what features we plan on developing. This is our rough plan for the first ~3ish months.

2d engine -> Integrated Server -> Code Generation -> Web IDE -> Polish & MVP


_Note: You'll notice that the earlier are more detailed than the later parts. As we finish up sections we get a better understanding for what we need to do in the next section and we can add futher details at that time._


## 2d engine
_2 Weeks_

First we are going to make a simple 2d engine to start with that compiles to Web Assembly(WASM). Since we are just working towards an MVP it is just going to do the basics for right now and we can add aditional features after the MVP is finished and we are working towards an Alpha release.

You can alread see most of the engine features are planned and you can check our progress by following the issues on [Gitlab](https://gitlab.com/manaengine/roadmap/-/boards). These include

- Compile to WASM
- Drawabe shapes
  - Basic Shapes
  - Matrix Transformations
- Input & Event Loop
  - Working Event Loop
  - Input Detection
- Views
  - Camera
  - World Coordinates
- Textures
  - Images
  - Animation
  - Image Atlas
- Physics
  - Collision Detection
- Demo
  - Simple Demo Game


## Integrated Server
_2-3 Weeks_

After our engine is working we need to add the multiplayer part of our platform. For now we just need the basics working and can worry about persistence when we head towards our alpha. Now we are just focusing on getting multiple clients to talk to each other and handle all of the complications that come with real time communication. The issues we are looking at are

- Connections
  - Get the clients and servers talking to eachother
  - Timeouts
  - Convert updates to binary for transmission
  - Websockets (because browsers)
- Client Side Prediction
  - Server Reconciliation
- Entity Interpolation
- Lag compensation
- Demo
  - Add multiplayer server to our earlier demo

## Code Generation
_3 Weeks_

This is the meat behind out WebIDE. When a user drags and drops a component into their game it triggers a code generation event that writes all the needed code and integrates it into their game without them having to think about it. For now we just need to plan out our API and implement a few basic components for the MVP.

- Components
  - Characters
  - Movement
    - Controlled
    - Automated
  - Scoring
  - Background/Textures
  - Map object
    - interactable
    - solid
    - move through
- Code Generation Macros
  - Think out how we want our API to work
- Entity Interpolation
- Lag compensation
- Demo
  - Create our game from an emtpy template just using generated code


## Web IDE
_3-4 Weeks_

This provides a REPL like enviorment for the user to make their game. Since we are just focusing on an MVP right now we don't need to worry about accounts/login yet but just making sure we have the core functionality working. This includes

- REPL Environment
  - Automatic reloading
  - Figure out basic UI/UX design
- Components
  - Add drag and drop feature
  - Hook up to code generation
- Demo
  - Create our game entirly from the browser

## Polish & MVP

We made it!!! We already have working prototype so we just need to polish up the edges and start getting some actual user feed back.


## After

From here we can start working on an alpha release which would include adding features to our engine, adding persistence to our servers, cleaning up our Web IDE, adding account features to our website, and probably countless other features.
